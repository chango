from django.db import models

from django.contrib import admin


class Thread(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    # TODO: get max_length from global bbs settings
    subject = models.CharField(max_length=128)
    sticky = models.BooleanField()
    opened = models.BooleanField()
    
    def __unicode__(self):
        # TODO: remove because this method is useless?
        return '[{0}] {1}'.format(self.id, self.subject)
    
    @models.permalink
    def get_absolute_url(self):
        return ('thread_view', (), {
            'thread_id': self.id,
        })
    
    # TODO: set `object` as superclass?
    # TODO: more meta stuff!
    class Meta:
        ordering = ['-sticky', '-creation_date']

class ThreadAdmin(admin.ModelAdmin):
    fields = ('sticky', 'opened', 'subject')
    list_display = ('id', 'subject', 'opened')


class Message(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    # TODO: set the thread attribute as some kind of index for faster access?
    thread = models.ForeignKey(Thread)
    text = models.TextField()
    
    # TODO: return "first chars of text..." or "self.thread.subject > self.text"
    def __unicode__(self):
        # TODO: remove because this method is useless?
        return '[{0}] {1}'.format(self.id, self.text)
    
    # TODO: create a permalink?
    def get_absolute_url(self):
        return '/thread/%i#%i' % (self.thread.id, self.id)
    
    class Meta:
        ordering = ['creation_date']
        # TODO: order_with_respect_to = 'thread'?

class MessageAdmin(admin.ModelAdmin):
    fields = ('thread', 'text',)
    list_display = ('id', 'text')


admin.site.register(Thread, ThreadAdmin)
admin.site.register(Message, MessageAdmin)
