from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_protect

from models import Thread, Message


def threads(request):
    return render_to_response('bbs/threads.html', {
        'threads': Thread.objects.all(),
    })


# TODO: check thread_id with try except
def thread(request, thread_id):
    return render_to_response('bbs/thread.html', {
        'thread': Thread.objects.get(id=thread_id),
        # TODO: sort messages by creation_date
        'messages': Message.objects.filter(thread=thread_id),
    })


# TODO: only mods can create sticky threads
@csrf_protect
def new_thread(request):
    from django.template import RequestContext
    from django.http import HttpResponseRedirect
    from bbs.forms import NewThreadForm
    
    if request.method == 'POST':
        form = NewThreadForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            text = form.cleaned_data['text']
            # TODO: create new Thread and Message atomically?
            newThread = Thread(subject=subject, sticky=False)
            newThread.save()
            newMessage = Message(thread=newThread, text=text)
            newMessage.save()
            # TODO: redirect to new thread
            return HttpResponseRedirect('/')
    else:
        form = NewThreadForm()
    context = RequestContext(request, {
        'form': form,
    })
    return render_to_response('bbs/new_thread.html', context)
