from django import template
register = template.Library()

from django.template.defaultfilters import stringfilter

@stringfilter
@register.filter
def xxx(value):
    "replace last 3 digits by XXX"
    return value[:-3] + 'XXX'
# xxx.is_safe = True # TODO: check this