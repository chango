from django import forms

# TODO: add username, tripcode, email, ...
class NewThreadForm(forms.Form):
    # TODO: get max_length from global bbs settings
    subject = forms.CharField(max_length=128)
    text = forms.CharField(widget=forms.Textarea)
