# README

chango is a Tinychan clone powered by the Django framework.

- [official project page](http://github.com/exterlulz/chango)
- [git mirror](http://repo.or.cz/w/chango.git)

Don't hesitate to post any bug, strange behavior, or suggestion in the [issue tracker](http://github.com/exterlulz/chango/issues)

# Installation

1. install [Django 1.2](http://www.djangoproject.com/download/1.2/tarball/)
2. install the chango project in its own directory
3. create the DB and a superuser account: `python manage.py syncdb`
4. [optional] import some default threads and messages: `python manage.py loaddata bbs/fixtures.json`
5. run the server: `python manage.py runserver`

# Features

- No real security yet! CSRF protection is enabled though...
- Google fonts

# Notes

- chan template filters:
  - xxx
- bbs is the text board application
- templatetags must be inside an app, it contains all the custom tags and filters that can be used later with {% load filters %}
