# TODO

## Next

- how can I deduce a message's link from a thread's link?
- reply to message
- create fixture for the FAQ

## Misc

- is Django's comments framework useful?
- create models directory, split models in their own files
- new\_thread
  - clean new\_thread view, especially the Thread and Message creation
  - clean form error messages for validation
  - check "form form" from tinychan: table, p, or ul?
- datetime for threads and messages
  - timesince/timeuntil filters for each message in a thread
  - using JS only? (would be less CPU intensive)
- add a title to all the pages
- clean whitespace
- add newline at the end of each python script
- use striptags filter for user provided content
- clean format strings everywhere with .format() method
- some admin function to swap message IDs and force a number
- relative time for messages (3 hours ago...)
- remove whitespace in thread's subject
- create modules for pass, tripcode generation
  - generate_password(): # import random
        chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        return ''.join(random.sample(chars, 32))
  - generate uuid(): # import uuid
        return uuid.uuid1() # or uuid.uuid4() for more random
- check files mode (0755??) if it matters
- create INSTALL file for procedures to follow
  - Django files to fill DB with default values (don't remember how)
  - urls.py, settings.py, timezone, ...
- setup new remote on exterlulz.net, and push on both servers at once
- id/password generated with uuid.uuid1()
- tripcodes (hashlib...)
- ignore list (ostrich mode?) for each user
- very basic security (Apache (.htaccess), Nginx (allow/deny), Lighttpd) to prevent people from seeing the admin section, only allow from localhost
- use django.contrib.markup with Markdown, Textile, or RestructuredText
- compare Markdown with WakabaMark and Tinychan's markup
- name anons in each thread with default: A..Z...
- show 25 posts in each page, set in admin (site wide) or in user interface
- captcha with predefined images
- clean code by adding/removing blank line at the end of each file
- clean code with Google's code conventions
- cite should display the name in brackets next to the post number
- panic mode
- check how to remove the content tag from repo.or.cz
- b& page with a JS timer, b& application
- check the security of forms!
- protect views' input values with `try int(val) except...`

## Spam

- spam list
- report message
- regular expressions
- anti flood system
- proxies autoban
- efnet rbl list

## Filters

- message: strip all html -> markup -> filter urls
- check if the xxx filter is *really* safe
- change xxx to some kind of intfilter
- reverse
- word filters
- backward header filter (name, reply, link...)

## Design

- themes
  - 4chan
  - plus4chan
  - tinychan
  - lain
  - christmas
- misc css
  - b& posts
  - cell phones
- change themes in real-time like 4-ch.net

## Admin interface

- mix thread and messages in admin interface
- quick delete, ban, filter, nuke (ionize)
- install docutils for admin documentation, and enable in urls.py and settings.py
- max length of the thread's subject should be configurable
- set captcha on/off from admin zone
- CSS and customizations with ADMIN\_MEDIA\_PREFIX
- search box
- word filters
- mass delete
- graphical installer
- add or remove bulletins
- content manager from the admin interface
- ban
  - mass ban
  - appeal page
  - presets
  - stealth ban (random errors)
  - full block
- monitoring
  - b& people
  - suspicious activity
  - stat center
- moderators
  - restricted admin page
  - wiseguy activity and operation history
- admin.ModelAdmin?

## Django

- use filters like title, slug, or random case
- can one app call the templatetags of another app

## Future ideas

- clean app according to http://code.djangoproject.com/wiki/DosAndDontsForApplicationWriters
- create good link to threads like /123/#123 and check with Django
- remove Google fonts for "mobile template"
- find a way to remove example.com in the default DB
- cache templates with django.template.loaders.cached.Loader
- i18n for strings
- special pages with "filters" : today's messages...
- an ImageField that can be "null" for each message
- tripcodes caching for faster creation
- have the ability to quote multiple posts at once
- different categories (/b/, programming, news, ...)
- 2 modes (2 templates?): tinychan (list of subjects only), wakaba (list on top)
- create an image board app next to the bbs
- simple JS with coffeescript and jquery
- haml and sass in preprocessing templates
- ajax replies
- picture storage with database (sqlite, mongodb, ...)
- automatic db upgrade
- list bans in public page
- html div in template for advertising (like flattr or openx), and admin page to enable/disable ads
- tagging system
- for each topic (and/or reply) :
  - add a collection of key/values (JSON-style) like tags
  - tags could be used to "customize" threads (message board, bug tracker, calendar, ...)
- uwsgi
- poll system
- remove the concept of "project." a standalone app should be sufficient
- remove the Content tags on repo.or.cz ASAP
- forced anon
- spam trap

## Links

- [coffeescript](http://jashkenas.github.com/coffee-script/)
- [jquery](http://jquery.com/)
- [haml](http://haml-lang.com/)
- [sass](http://sass-lang.com/)
- [flattr](http://flattr.com/)
- [openx](http://www.openx.org/)
