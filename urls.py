from django.conf.urls.defaults import *

from django.contrib import admin
admin.autodiscover()

import bbs.views


urlpatterns = patterns('',
    (r'^admin/', include(admin.site.urls)),
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    
    (r'^$', bbs.views.threads),
    (r'^threads/$', bbs.views.threads),
    url(r'^thread/(?P<thread_id>\d+)', bbs.views.thread, name='thread_view'),
    (r'^new_thread/$', bbs.views.new_thread),
)
